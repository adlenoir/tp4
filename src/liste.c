#include "liste.h"

/* -------------------------------------------------------------------- */
/* CreerCellule   														                          */
/* 		Crée une cellule contenant une clé et une valeur du dictionnaire  */
/*                                                                      */
/* En entrée: cle, valeur deux chaine de caracteres associant un mot    */
/*			       à sa traduction        									                */
/*                                                                      */
/* En sortie: Retourne l'adresse de la cellule créée			              */
/*                                                                      */
/* Local : tmp : pointeur temporaire sur la nouvelle cellule créée      */
/* -------------------------------------------------------------------- */
cellule_t *CreerCellule(char *cle, char *valeur){
    cellule_t *tmp = (cellule_t*)malloc(sizeof(cellule_t));
    if(tmp != NULL){
        strcpy(tmp->cle, cle);
        strcpy(tmp->valeur, valeur);
        tmp->suiv = NULL;
    }
    return tmp;
}

/* -------------------------------------------------------------------- */
/* Inserer          Insère une cellule dans la liste chaînée            */
/*                                                                      */
/* En entrée: cellule : pointeur sur la cellule que l'on ajoute 		    */
/* 					  dans la liste                         		              	*/
/*            list : pointeur de pointeur sur la première cellule de    */
/*                      la sous table                                   */
/*                                                                      */
/* En sortie:       												                            */
/* -------------------------------------------------------------------- */
void Inserer(donnee_t* liste, cellule_t* cellule){
    cellule->suiv = *liste;
    *liste = cellule;
}

/* -------------------------------------------------------------------- */
/* AfficherListe          Affiche la liste (sous table)                 */
/*                                                                      */
/* En entrée: liste : pointeur sur lé première cellule                  */
/*                                                                      */
/* En sortie: 													                                */
/*                                                                      */
/* Local : cour : pointeur courant sur la liste                         */
/* -------------------------------------------------------------------- */
void AfficherListe(donnee_t liste){
    cellule_t *cour = liste;
    while(cour != NULL){
        printf("%s %s\n", cour->cle, cour->valeur);
        cour = cour->suiv;
    }
}

/* -------------------------------------------------------------------- */
/* LibererListe          Libère la mémoire allouée pour la liste        */
/*                                                                      */
/* En entrée: liste : pointeur sur la liste de cellules (sous table)    */
/*                                                                      */
/* En sortie: 													                                */
/*                                                                      */
/* Local : cour : pointeur courant sur la liste                         */
/*         tmp : pointeur temporaire sur la liste                       */
/* -------------------------------------------------------------------- */
void LibererListe(donnee_t liste){
  cellule_t *cour = liste, *tmp;
	while(cour != NULL)
	{
    tmp = cour;
    cour = cour->suiv;
		free(tmp);
	}
}
