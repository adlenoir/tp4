#include "table.h"

/* -------------------------------------------------------------------- */
/* main        Programme principal                                      */
/*                                                                      */
/* En entrée: argc : taille du tableau argv                             */
/*            argv : tableau des arguments du programme                 */
/*                                                                      */
/* En sortie: code de retour de la fonction pour le processus entier    */
/*																																			*/
/* Local : choix : entier représentant le choix de langue								*/
/*				 fichier : contient le nom du fichier à charger								*/
/*				 table : pointeur sur une table 															*/
/* -------------------------------------------------------------------- */
int main(int argc, char **argv)
{
	int choix=0;
	char fichier[50], test;
	Table* table = CreerTable();

	puts("Souhaitez vous lancer des test ? (y/N)");
	fscanf(stdin,"%c", &test);
	if(test == 'y') {
		puts("----------------- CHARGEMENT -----------------");
		puts("Chargement: Fichier vide");
		strcpy(fichier, "src/dico.txt");
		printf("Code de retour de chargement : %d\n", Chargement(table, fichier));
		AfficherTable(table);

		puts("Chargement: Cas général");
		strcpy(fichier, "src/francais.txt");
		printf("Code de retour de chargement : %d\n", Chargement(table, fichier));
		AfficherTable(table);
		puts("----------------- FIN CHARGEMENT -----------------");

		LibererTable(table);
		table = CreerTable();

		puts("----------------- RECHERCHE -----------------");
		puts("Recherche: table vide");
		printf("Résultat de la recherche : %p\n", (void*)Recherche(table, "toto"));

		Chargement(table, fichier);

		puts("Recherche: en entrée un mot qui n’existe pas dans la table");
		printf("Résultat de la recherche : %p\n", (void*)Recherche(table, "toto"));

		puts("Recherche: en entrée un mot qui existe (ex: hello)");
		printf("Résultat de la recherche : %s\n", Recherche(table, "hello")->valeur);

		puts("Recherche: en entrée un mot qui existe à la fin d’une liste chaînée (ex: good)");
		printf("Résultat de la recherche : %s\n", Recherche(table, "good")->valeur);

		puts("----------------- FIN RECHERCHE -----------------");

		LibererTable(table);
		table = CreerTable();

		puts("----------------- TRADUCTION -----------------");
		if(argc < 2){
			printf("\nVous n'avez pas écrit en argument les mots à traduire !\n");
		}
		else {
			puts("Traduction: table vide");
			Traduction(table, argv);

			Chargement(table, fichier);

			puts("Traduction: Mot existant dans le début d’une liste chaînée, fin d'une liste chaînée et inexistant");
			Traduction(table, argv);
		}

		puts("----------------- FIN TRADUCTION -----------------");

		LibererTable(table);
		table = CreerTable();

		puts("----------------- AFFICHAGE -----------------");
		puts("Affichage: table vide");
		AfficherTable(table);

		strcpy(fichier, "src/espagnol.txt");
		Chargement(table, fichier);

		puts("Affichage: Table avec plusieurs cellules par liste chaînée");
		AfficherTable(table);

		puts("----------------- FIN AFFICHAGE -----------------");

		LibererTable(table);
	}
	else {
		if(argc < 2){
			printf("\nVous n'avez pas écrit en argument les mots à traduire !\n");
		}
		else{
			++argv;
			strcpy(fichier,"src/");
			while(choix < 1 || choix > 3){
				printf("En quelle langue souhaitez-vous traduire ?\n");
				printf("1)Français\n");
				printf("2)English\n");
				printf("3)Español\n");
				fscanf(stdin,"%d",&choix);
				system("clear");
				switch (choix) {
					case 1:strcat(fichier,"francais.txt");break;
					case 2:strcat(fichier,"anglais.txt");break;
					case 3:strcat(fichier,"espagnol.txt");break;
					default: printf("\nLe choix saisi n'est pas disponible !\n");
				}
			}
			if(Chargement(table,fichier)==0){
				Traduction(table,argv);
				printf("\n");
				LongMoy(table);
			}
		}
		LibererTable(table);
	}
	return 0;
}
