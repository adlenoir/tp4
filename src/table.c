#include "table.h"

/* -------------------------------------------------------------------- */
/* CreerTable          		Creer une table vide 	                        */
/*                                                                      */
/* En entrée:								                                            */
/*                                                                      */
/* En sortie: L'adresse d'une table correctement initialisée		        */
/*                                                                      */
/* Local : i : variable d'itération                                     */
/*         table : pointeur sur la table                                */
/* -------------------------------------------------------------------- */
Table* CreerTable() {
  int i;
  Table* table = NULL;

  table = (Table*)malloc(sizeof(Table));
  if(table != NULL){
    for(i = 0; i < HASH_MAX; ++i) {
      table->table_majeur[i] = NULL;
    }
    table->compteur = 0;
  }
  return table;
}

/* -------------------------------------------------------------------- */
/* Chargement   Lis et ajoute les données d'un fichier dans la table    */
/*                                                                      */
/* En entrée: table : pointeur sur la table							                */
/* 			      fichier : nom du fichier à lire                           */
/*                                                                      */
/* En sortie: Code de retour de la fonction       						          */
/*                                                                      */
/* Local : flot : pointeur de fichier                                   */
/*         code : code de retour de la fonction                         */
/*         line : chaîne de caractères qui va recevoir                  */
/*                  la ligne lue dans le fichier                        */
/*         cle : chaîne de caractères contenant la clé à insérer        */
/*         valeur : chaîne de caractères contenant la valeur à insérer  */
/*         tmp : pointeur temporaire sur la nouvelle cellule            */
/* -------------------------------------------------------------------- */
int Chargement(Table *table, const char *fichier){
    FILE *flot;
    int code;
    char line [100], *cle, *valeur;
    cellule_t *tmp;

    flot = fopen(fichier, "r");
    if(!flot){
        fprintf(stderr, "Impossible d'ouvrir le fichier.\n");
        code = EXIT_FAILURE;
    }
    else {
        fgets(line, 100, flot);
        while(!feof(flot)){
            if(line[strlen(line)-1] == '\n')
                line[strlen(line)-1]='\0';

            cle = strtok(line, ";");
            valeur = strtok(NULL, ";");

            tmp = CreerCellule(cle, valeur);
            ++table->compteur;
            Inserer(&(table->table_majeur[hash_string(tmp->cle)]), tmp);
            fgets(line, 100, flot);
        }
        code = EXIT_SUCCESS;
        fclose(flot);
    }
    return code;
}

/* -------------------------------------------------------------------- */
/* Recherche   Recherche la valeur pour une clé donnée, dans la table   */
/*                                                                      */
/* En entrée: table : pointeur sur la table 						                */
/* 			      cle : clé à rechercher                                    */
/*                                                                      */
/* En sortie: Adresse de la cellule trouvé sinon NULL					          */
/*                                                                      */
/* Local : cour : pointeur courant sur une cellule                      */
/* -------------------------------------------------------------------- */

donnee_t Recherche(Table * table, char *cle){
  donnee_t cour;

  cour = table->table_majeur[hash_string(cle)];
    while(cour != NULL && strcmp(cour->cle,cle)!=0)
      cour = cour->suiv;

  return cour;
}

/* -------------------------------------------------------------------- */
/* AfficherTable          Affiche la table                              */
/*                                                                      */
/* En entrée: table : pointeur sur la table à afficher                  */
/*                                                                      */
/* En sortie:                         													        */
/*                                                                      */
/* Local : i : variable d'itération                                     */
/* -------------------------------------------------------------------- */
void AfficherTable(Table* table){
    int i;
    puts("\n--------------------- DEBUT --------------------------");
    for(i = 0; i< 29; ++i){
        printf("Liste %d\n", i);
        AfficherListe(table->table_majeur[i]);
    }
    puts("--------------------- FIN --------------------------\n");
}

/* -------------------------------------------------------------------- */
/* Traduction                                                           */
/*        Traduit des mots à l'aide de la table qu'on lui donne         */
/*                                                                      */
/* En entrée: table : pointeur sur la table                             */
/*            mots : pointeur de pointeur sur les différents mots       */
/*                    à traduire                                        */
/*                                                                      */
/* En sortie:                         													        */
/*                                                                      */
/* Local : cour : pointeur courant sur une cellule                      */
/* -------------------------------------------------------------------- */
void Traduction(Table * table, char **mots){
  donnee_t cour;
  int i=0;

  while (*(mots+i) != NULL) {
    cour=Recherche(table, mots[i]);
  	if(cour != NULL)
  		printf("%s ",cour->valeur);
  	else
  		printf("... ");
    ++i;
  }
  printf("\n");
}

/* -------------------------------------------------------------------- */
/* LongMoy                                                               */
/*        Calcule la longueur moyenne des sous tables                   */
/*                                                                      */
/* En entrée: table : pointeur sur la table                             */
/*                                                                      */
/* En sortie:                         													        */
/*                                                                      */
/* Local : moy : longueur moyenne des sous tables                       */
/* -------------------------------------------------------------------- */
void LongMoy(Table * table){
  float moy;

  moy = (float)table->compteur/HASH_MAX;

  printf("La longueur moyenne des sous tables est de %.2f\n", moy);
}

/* -------------------------------------------------------------------- */
/* LibererTable                                                         */
/*        Libére de la memoire la structure de données de la table      */
/*                                                                      */
/* En entrée: table : pointeur sur la table                             */
/*                                                                      */
/* En sortie:                         													        */
/*                                                                      */
/* Local : i : varibale d'itération                                     */          
/* -------------------------------------------------------------------- */
void LibererTable(Table * table) {
  int i;

  if(table != NULL){
    for(i = 0; i < HASH_MAX; ++i) {
      LibererListe(table->table_majeur[i]);
    }
  }
  free(table);
}
