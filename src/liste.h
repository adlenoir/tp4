#ifndef LISTE_H
#define LISTE_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* -------------------------------------------------------------------- */
/* 						Fichier d'entête des fonctions d'une liste        				*/
/*                                                                			*/
/* Contient les prototypes et la structure d'une liste                  */
/*																		                                  */
/* -------------------------------------------------------------------- */

typedef struct cellule {
    char    cle[40];
    char    valeur[40];
    struct  cellule* suiv;
} cellule_t, *donnee_t;

cellule_t   *CreerCellule(char *, char*);
void        Inserer(donnee_t *, cellule_t *);
void        Supprimer(cellule_t *, cellule_t **);
void        AfficherListe(donnee_t);
void        LibererListe(donnee_t);

#endif
