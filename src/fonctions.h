/* -------------------------------------------------------------------- */
/* 						           Fichier d'entête fonctions				              */
/*                                                             			    */
/* Contient les prototypes des fonctions autres.						            */
/*																		                                  */
/* -------------------------------------------------------------------- */

#ifndef FONCTIONS
#define FONCTIONS
#include <ctype.h>
#define HASH_MAX 29

unsigned int hash_string(const char *str);

#endif
