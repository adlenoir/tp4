#ifndef TABLES
#define TABLES
/* -------------------------------------------------------------------- */
/* 						Fichier d'entête des fonctions de la table        				*/
/*                                                                			*/
/* Contient les prototypes et la structure de la table                  */
/*																		                                  */
/* -------------------------------------------------------------------- */

#include "fonctions.h"
#include "liste.h"

typedef struct table {
    donnee_t table_majeur [HASH_MAX];
    int compteur;
} Table;

int Chargement(Table *, const char *);
Table* CreerTable();
void AfficherTable(Table *);
donnee_t Recherche(Table * table, char *cle);
void Traduction(Table * table, char **mots);
void LongMoy(Table * table);
void LibererTable(Table * table);

#endif
