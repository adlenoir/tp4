#include "fonctions.h"

/* -------------------------------------------------------------------- */
/* hash_string          		Creer une liste vide 	                      */
/*                                                                      */
/* En entrée:	str : chaîne de caractères sur pour générer               */
/*                  la clé de hachage					                          */
/*                                                                      */
/* En sortie: Un entier représentant l'indice de la table majeur        */
/* -------------------------------------------------------------------- */
unsigned int hash_string(const char *str)
{
  unsigned int hash = 5381;                /*  fonction de hachage de D.J. Bernstein*/
  const char *s;
  for (s = str; *s; s++)
 	 hash = ((hash << 5) + hash) + tolower(*s);
  return (hash & 0x7FFFFFFF)%HASH_MAX;
}
